﻿namespace ApplicationContracts.ViewModels.Application.Priority
{
    public class DeletePriorityViewModel
    {
        public int Id { get; set; }
    }
}