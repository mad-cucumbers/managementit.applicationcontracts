﻿namespace ApplicationContracts.ViewModels.Application.Priority
{
    public class PriorityByIdRequest
    {
        public int Id { get; set; }
    }
}