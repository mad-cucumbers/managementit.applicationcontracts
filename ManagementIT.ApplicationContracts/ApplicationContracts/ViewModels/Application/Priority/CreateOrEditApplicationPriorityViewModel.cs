﻿namespace ApplicationContracts.ViewModels.Application.Priority
{
    public class CreateOrEditApplicationPriorityViewModel
    {
        public string Name { get; set; }
        public bool IsDefault { get; set; }
    }
}