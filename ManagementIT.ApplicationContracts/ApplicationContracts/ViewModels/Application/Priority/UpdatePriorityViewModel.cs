﻿using System.ComponentModel.DataAnnotations;

namespace ApplicationContracts.ViewModels.Application.Priority
{
    public class UpdatePriorityViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public bool IsDefault { get; set; }
    }
}