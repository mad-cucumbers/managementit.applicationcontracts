﻿namespace ApplicationContracts.ViewModels.Application.Priority
{
    public class ApplicationPriorityViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }

        public ApplicationPriorityViewModel() { }
    }
}