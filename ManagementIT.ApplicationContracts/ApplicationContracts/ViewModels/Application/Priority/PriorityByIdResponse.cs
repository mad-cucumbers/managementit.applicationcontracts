﻿using Contracts.ResponseModels;

namespace ApplicationContracts.ViewModels.Application.Priority
{
    public class PriorityByIdResponse
    {
        public ApplicationPriorityViewModel model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}