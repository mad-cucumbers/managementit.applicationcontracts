﻿using System.Collections.Generic;
using Contracts.ResponseModels;

namespace ApplicationContracts.ViewModels.Application.Priority
{
    public class AllPriorityResponse
    {
        public IEnumerable<ApplicationPriorityViewModel> priorities { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}