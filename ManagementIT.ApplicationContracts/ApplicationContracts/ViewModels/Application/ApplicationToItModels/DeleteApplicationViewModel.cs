﻿namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class DeleteApplicationViewModel
    {
        public int Id { get; set; }

        public string IniciatorUserName { get; set; }
    }
}