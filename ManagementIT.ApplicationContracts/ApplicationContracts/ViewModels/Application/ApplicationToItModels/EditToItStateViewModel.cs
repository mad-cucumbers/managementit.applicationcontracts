﻿using System.ComponentModel.DataAnnotations;

namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class EditToItStateViewModel
    {
        [Required]
        public int AppId { get; set; }
        [Required]
        public int StateId { get; set; }

        public string IniciatorUserName { get; set; }
    }
}