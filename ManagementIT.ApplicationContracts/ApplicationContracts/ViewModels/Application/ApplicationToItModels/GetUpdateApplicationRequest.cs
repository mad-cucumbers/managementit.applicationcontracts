﻿namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class GetUpdateApplicationRequest
    {
        public int ApplicationId { get; set; }
    }
}