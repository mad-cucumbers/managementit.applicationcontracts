﻿namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class GetApplicationByDeptIdRequest
    {
        public int DeptId { get; set; }
    }
}