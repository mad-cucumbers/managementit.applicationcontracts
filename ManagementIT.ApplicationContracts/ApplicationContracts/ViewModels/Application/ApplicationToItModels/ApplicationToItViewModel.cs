﻿using ApplicationContracts.ViewModels.Application.Priority;
using ApplicationContracts.ViewModels.Application.StateModels;
using ApplicationContracts.ViewModels.Application.TypeModels;
using Contracts.ViewModels.Application.StateModels;

namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class ApplicationToItViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ApplicationPriorityViewModel Priority { get; set; }
        public ApplicationTypeViewModel Type { get; set; }
        public string Content { get; set; }
        public string Note { get; set; }
        public int DepartamentId { get; set; }
        public int RoomId { get; set; }
        public int EmployeeId { get; set; }
        public string Contact { get; set; }
        public ApplicationStateViewModel State { get; set; }

        public string EmployeeFullName { get; set; }
        public string RoomName { get; set; }
        public string DepartamentName { get; set; }

        public ApplicationToItViewModel() { }
    }
}