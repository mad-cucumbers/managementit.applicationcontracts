﻿using System.ComponentModel.DataAnnotations;

namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class OnDeleteApplicationViewModel
    {
        [Required]
        public int Id { get; set; }

        public string IniciatorUserName { get; set; }
    }
}