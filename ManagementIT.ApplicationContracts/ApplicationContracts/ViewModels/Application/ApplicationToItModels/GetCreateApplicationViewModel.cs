﻿using System.Collections.Generic;
using ApplicationContracts.ViewModels.Application.Priority;
using ApplicationContracts.ViewModels.Application.StateModels;
using ApplicationContracts.ViewModels.Application.TypeModels;
using Contracts.ViewModels.Application.StateModels;

namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class GetCreateApplicationViewModel
    {
        public string Name { get; set; }
        public string Content { get; set; }
        public string Note { get; set; }
        public string Contact { get; set; }
        public int ApplicationTypeId { get; set; }
        public int ApplicationPriorityId { get; set; }
        public int DepartamentId { get; set; }
        public int RoomId { get; set; }
        public int EmployeeId { get; set; }
        public int StateId { get; set; }


        public List<ApplicationPriorityViewModel> SelectPriority { get; set; }
        public List<ApplicationTypeViewModel> SelectType { get; set; }
        public List<ApplicationStateViewModel> SelectState { get; set; }

        public GetCreateApplicationViewModel() { }
    }
}