﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class UpdateRoomNameRequest
    {
        public int RoomId { get; set; }
        public string RoomName { get; set; }
    }
}
