﻿using Contracts.ResponseModels;

namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class ApplicationByIdResponse
    {
        public ApplicationToItViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}