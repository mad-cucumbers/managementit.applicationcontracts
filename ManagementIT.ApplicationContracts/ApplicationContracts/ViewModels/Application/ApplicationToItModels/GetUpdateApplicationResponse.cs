﻿using Contracts.ResponseModels;

namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class GetUpdateApplicationResponse
    {
        public UpdateApplicationViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}