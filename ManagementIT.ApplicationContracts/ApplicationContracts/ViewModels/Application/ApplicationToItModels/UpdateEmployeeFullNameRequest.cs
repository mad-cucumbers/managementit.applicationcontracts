﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class UpdateEmployeeFullNameRequest
    {
        public int EmployeeId { get; set; }
        public string EmployeeFullName { get; set; }
    }
}
