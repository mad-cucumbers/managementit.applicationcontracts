﻿using System.Collections.Generic;
using Contracts.ResponseModels;

namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class AllApplicationResponse
    {
        public IEnumerable<ApplicationToItViewModel> Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}