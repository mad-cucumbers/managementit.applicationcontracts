﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class EditPriorityOrApplicationViewModel
    {
        public int AppId { get; set; }
        public int PriorityId { get; set; }
        public string IniciatorUserName { get; set; }
    }
}
