﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class UpdateDepartmentNameRequest
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
    }
}
