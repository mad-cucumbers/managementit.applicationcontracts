﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ApplicationContracts.ViewModels.Application.Priority;
using ApplicationContracts.ViewModels.Application.StateModels;
using ApplicationContracts.ViewModels.Application.TypeModels;
using Contracts.ViewModels.Application.StateModels;

namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class UpdateApplicationViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Content { get; set; }
        public string Note { get; set; }
        public string Contact { get; set; }
        [Required]
        public int ApplicationTypeId { get; set; }
        [Required]
        public int ApplicationPriorityId { get; set; }
        [Required]
        public int DepartamentId { get; set; }
        [Required]
        public int RoomId { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        [Required]
        public int StateId { get; set; }
        public ApplicationTypeViewModel Type { get; set; }
        public ApplicationStateViewModel State { get; set; }
        public ApplicationPriorityViewModel Priority { get; set; }

        public string EmployeeFullName { get; set; }
        public string RoomName { get; set; }
        public string DepartamentName { get; set; }


        public string IniciatorUserName { get; set; }


        public List<ApplicationPriorityViewModel> SelectPriority { get; set; }
        public List<ApplicationTypeViewModel> SelectType { get; set; }
        public List<ApplicationStateViewModel> SelectState { get; set; }
    }
}