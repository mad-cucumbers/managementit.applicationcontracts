﻿namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class ApplicationByIdRequest
    {
        public int Id { get; set; }
    }
}