﻿using Contracts.ResponseModels;

namespace ApplicationContracts.ViewModels.Application.ApplicationToItModels
{
    public class GetCreateApplicationResponse
    {
        public CreateApplicationToITViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}