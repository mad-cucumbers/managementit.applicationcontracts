﻿using Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationContracts.ViewModels.Application.ActionModels
{
    public class ActionByEnumTypeRequest
    {
        public int NumberType { get; set; }
    }
}
