﻿using Contracts.ResponseModels;

namespace ApplicationContracts.ViewModels.Application.ActionModels
{
    public class ActionByIdResponse
    {
        public ActionViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}