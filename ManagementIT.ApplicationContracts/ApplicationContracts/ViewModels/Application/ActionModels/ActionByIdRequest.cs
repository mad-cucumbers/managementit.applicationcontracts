﻿using Contracts.Enums;

namespace ApplicationContracts.ViewModels.Application.ActionModels
{
    public class ActionByIdRequest
    {
        public int ActionId { get; set; }
        public int NumberType { get; set; }
    }
}