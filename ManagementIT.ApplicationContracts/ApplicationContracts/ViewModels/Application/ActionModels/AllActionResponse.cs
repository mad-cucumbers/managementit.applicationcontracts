﻿using System.Collections.Generic;
using Contracts.ResponseModels;

namespace ApplicationContracts.ViewModels.Application.ActionModels
{
    public class AllActionResponse
    {
        public IEnumerable<ActionViewModel> Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}