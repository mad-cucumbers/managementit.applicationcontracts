﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationContracts.ViewModels.Application.ActionModels
{
    public class DeleteSelectActionRequest
    {
        public List<int> IdsAction { get; set; }
    }
}
