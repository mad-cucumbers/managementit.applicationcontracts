﻿namespace ApplicationContracts.ViewModels.Application.ActionModels
{
    public class ActionViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AppId { get; set; }
        public string StateName { get; set; }
        public string UserNameIniciator { get; set; }
        public string DateOrTime { get; set; }
        public int DeptId { get; set; }
        public string ContentApp { get; set; }
        public string Action { get; set; }
    }
}