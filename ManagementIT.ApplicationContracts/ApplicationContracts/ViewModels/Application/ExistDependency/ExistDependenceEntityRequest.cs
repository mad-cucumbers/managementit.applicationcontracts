﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationContracts.ViewModels.Application.ExistDependency
{
    public class ExistDependenceEntityRequest
    {
        public int RoomId { get; set; }
        public int DepartmentId { get; set; }
        public int EmployeeId { get; set; }
        public ExistDependenceEntityRequest(){}
        public ExistDependenceEntityRequest(int roomId = 0, int departmentId = 0, int employeeId = 0)
        {
            RoomId = roomId;
            DepartmentId = departmentId;
            EmployeeId = employeeId;
        }
    }
}
