﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationContracts.ViewModels.Application.ExistDependency
{
    public class ExistDepandencyEntityResponse
    {
        public bool Exists { get; set; }
    }
}
