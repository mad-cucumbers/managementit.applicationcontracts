﻿using System.ComponentModel.DataAnnotations;

namespace ApplicationContracts.ViewModels.Application.StateModels
{
    public class UpdateStateViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string State { get; set; }
        public string BGColor { get; set; }
        public bool IsDefault { get; set; }
    }
}