﻿namespace ApplicationContracts.ViewModels.Application.StateModels
{
    public class ApplicationStateViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public string BGColor { get; set; }
        public bool IsDefault { get; set; }

        public ApplicationStateViewModel() { }
    }
}