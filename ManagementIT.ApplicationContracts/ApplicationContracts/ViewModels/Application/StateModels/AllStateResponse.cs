﻿using System.Collections.Generic;
using Contracts.ResponseModels;
using Contracts.ViewModels.Application.StateModels;

namespace ApplicationContracts.ViewModels.Application.StateModels
{
    public class AllStateResponse
    {
        public IEnumerable<ApplicationStateViewModel> Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}