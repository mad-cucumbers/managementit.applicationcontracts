﻿namespace ApplicationContracts.ViewModels.Application.StateModels
{
    public class DeleteStateViewModel
    {
        public int Id { get; set; }
    }
}