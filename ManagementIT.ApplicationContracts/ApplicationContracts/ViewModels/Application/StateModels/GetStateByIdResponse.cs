﻿using ApplicationContracts.ViewModels.Application.StateModels;
using Contracts.ResponseModels;

namespace Contracts.ViewModels.Application.StateModels
{
    public class GetStateByIdResponse
    {
        public ApplicationStateViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}