﻿namespace ApplicationContracts.ViewModels.Application.StateModels
{
    public class GetStateByIdRequest
    {
        public int Id { get; set; }
    }
}