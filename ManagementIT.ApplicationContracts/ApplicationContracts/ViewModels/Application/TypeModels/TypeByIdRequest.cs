﻿namespace ApplicationContracts.ViewModels.Application.TypeModels
{
    public class TypeByIdRequest
    {
        public int Id { get; set; }
    }
}