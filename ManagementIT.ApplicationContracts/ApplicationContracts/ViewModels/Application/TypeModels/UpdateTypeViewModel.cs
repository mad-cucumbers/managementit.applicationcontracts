﻿namespace ApplicationContracts.ViewModels.Application.TypeModels
{
    public class UpdateTypeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}