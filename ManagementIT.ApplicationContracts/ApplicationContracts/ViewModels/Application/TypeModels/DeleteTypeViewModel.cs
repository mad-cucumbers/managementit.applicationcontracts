﻿namespace ApplicationContracts.ViewModels.Application.TypeModels
{
    public class DeleteTypeViewModel
    {
        public int Id { get; set; }
    }
}