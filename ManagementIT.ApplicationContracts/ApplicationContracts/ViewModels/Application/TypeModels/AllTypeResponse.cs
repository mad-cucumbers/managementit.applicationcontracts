﻿using System.Collections.Generic;
using Contracts.ResponseModels;

namespace ApplicationContracts.ViewModels.Application.TypeModels
{
    public class AllTypeResponse
    {
        public IEnumerable<ApplicationTypeViewModel> model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}