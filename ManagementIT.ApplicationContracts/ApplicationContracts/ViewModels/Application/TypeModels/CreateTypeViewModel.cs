﻿using System.ComponentModel.DataAnnotations;

namespace ApplicationContracts.ViewModels.Application.TypeModels
{
    public class CreateTypeViewModel
    {
        [Required]
        public string Name { get; set; }
    }
}