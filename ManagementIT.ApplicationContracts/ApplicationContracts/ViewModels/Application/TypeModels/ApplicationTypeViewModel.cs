﻿namespace ApplicationContracts.ViewModels.Application.TypeModels
{
    public class ApplicationTypeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ApplicationTypeViewModel() { }
    }
}