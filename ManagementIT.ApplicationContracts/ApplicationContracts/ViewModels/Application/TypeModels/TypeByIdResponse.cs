﻿using Contracts.ResponseModels;

namespace ApplicationContracts.ViewModels.Application.TypeModels
{
    public class TypeByIdResponse
    {
        public ApplicationTypeViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}